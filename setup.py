#!/usr/bin/env python3

from setuptools import find_packages
from setuptools import setup

# PyPI dependencies should be put here
with open("./requirements.pip", "r") as f:
    requirements = f.read()

dependency_links = []

setup(
    name='deep_learning',
    setup_requires=['setuptools_scm'],
    use_scm_version=True,
    description="Skeleton for data science & machine learning projects",
    packages=find_packages(),
    test_suite='tests',
    install_requires=requirements,
    dependency_links=dependency_links,
    # include_package_data: to install data from MANIFEST.in
    include_package_data=True,
    scripts=['scripts/border_detector',
             'scripts/data_augmentation',
             'scripts/transfer_learning',
             'scripts/fine_tuning'],
    zip_safe=False,
)
