# Projet de Deep_learning / Vision par ordinateur (Hippolyte Lévêque & Louis Cassedanne)


Installation du code
====================

Ce projet comprend un Makefile qui vous permet d'installer très simplement tout le code écrit dans `deep_learning/` comme une nouvelle librairie python sur votre ordinateur, ainsi que toutes ses dépendances. Pour cela, lancez simplement::

    $ make install


Execution du code
=================

Si vous souhaitez tester les scripts d'augmentation de données et d'apprentissages, ceux-ci se trouvent tous dans le sous répertoire `scripts/`. Veuillez néanmoins noter que vous aurez alors besoin des données d'apprentissage à la racine du répertoire, nommé `data/`. 

Vous pouvez récupérer les données à cette adresse : http://vis-www.cs.umass.edu/lfw/

Nous avons simplement ajouté un dossier `data_sample/` pour tester les scripts dessus si nécessaires (il ne contient que très peu de données, mais a la même structure que le dossier data normal.

