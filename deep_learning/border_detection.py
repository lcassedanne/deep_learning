from PIL import Image
import cv2
import os
import ntpath
import numpy as np
from matplotlib import pyplot as plt


def border_detection(image_path, canny, laplacian, sobelx, sobely):
    if canny:
        canny_detection(image_path)
    if laplacian:
        laplacian_detection(image_path)
    if sobelx:
        sobelx_detection(image_path)
    if sobely:
        sobely_detection(image_path)



def canny_detection(image_path, down_threshold=100, up_threshold=200):
    """
    Input : ABSOLUTE path to a .png or .jpg file
    Output : Write at the exact same place as the image :
        - a new .jpg image with canny border detection
    """
    directory = os.path.dirname(image_path) 
    im_name = ntpath.basename(image_path).split('.')[0]
    img = cv2.imread(image_path, 0)
    canny = cv2.Canny(img,down_threshold,up_threshold)
    new_path = directory + "/" + im_name + "_canny.jpg" 
    cv2.imwrite(new_path, canny)
    

def laplacian_detection(image_path):
    """
    Input : ABSOLUTE path to a .png or .jpg file
    Output : Write at the exact same place as the image :
        - a new .jpg image with laplacian border detection
    """
    directory = os.path.dirname(image_path) 
    im_name = ntpath.basename(image_path).split('.')[0]
    img = cv2.imread(image_path, 0)
    laplacian = cv2.Laplacian(img,cv2.CV_64F)
    new_path = directory + "/" + im_name + "_laplacian.jpg" 
    cv2.imwrite(new_path, laplacian)



def sobelx_detection(image_path, ksize=3):
    """
    Input : ABSOLUTE path to a .png or .jpg file
    Output : Write at the exact same place as the image :
        - a new .jpg image with x_axis sobel border detection
    """
    directory = os.path.dirname(image_path) 
    im_name = ntpath.basename(image_path).split('.')[0]
    img = cv2.imread(image_path, 0)
    sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)
    new_path = directory + "/" + im_name + "_sobelx.jpg" 
    cv2.imwrite(new_path, sobelx)


def sobely_detection(image_path, ksize=3):
    """
    Input : ABSOLUTE path to a .png or .jpg file
    Output : Write at the exact same place as the image :
        - a new .jpg image with y_axis sobel border detection
    """
    directory = os.path.dirname(image_path) 
    im_name = ntpath.basename(image_path).split('.')[0]
    img = cv2.imread(image_path, 0)
    sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)
    new_path = directory + "/" + im_name + "_sobely.jpg" 
    cv2.imwrite(new_path, sobely)

