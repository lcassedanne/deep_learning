import torch
import torchvision
import numpy as np
from shutil import rmtree
import os
from torchvision import datasets, transforms, models
from sklearn.model_selection import train_test_split
import os
from deep_learning.utils import generate_sample_classes,train_model,evaluate,load_model,configure_optimizer
import torch.nn as nn
import torch.optim as optim




def fine_tuning_sample(root_directory,sample_nb,model='resnet',dropout=True,optimizer="SGD",epochs_nb=10):
    #chargement du réseau pré-entrainé
    print("Récupération du réseau pré-entraîné")
    convnet,cuda =  load_model(sample_nb,name=model,fine_tuning=True)
    if cuda:
        print('GPU')
    else:
        print('CPU')
    device = torch.device("cuda:0" if cuda else "cpu")
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    # on définit les transformations à appliquer aux images du dataset
    data_transforms = transforms.Compose([
        transforms.Resize([224, 224]),
        transforms.ToTensor(),
        transforms.Normalize(mean=mean, std=std)
    ])

    #generation de l'échantillon
    if 'temp_data_ft' in os.listdir():
        rmtree('temp_data_ft')
    generate_sample_classes(sample_nb,datadir=root_directory,target_dir="temp_data_ft/")
    image_directory = "temp_data_ft/"
    class_number = sample_nb
    print("Nombre de classes : %i" % class_number)
    dataset_full = datasets.ImageFolder(image_directory, data_transforms)
    loader_full = torch.utils.data.DataLoader(dataset_full, batch_size=16, shuffle=True, num_workers=4)

    # on split en train, val et test à partir de la liste des images
    np.random.seed(42)
    samples_train, samples_test = train_test_split(dataset_full.samples)
    samples_train, samples_val = train_test_split(samples_train)

    print("Nombre d'images de train : %i" % len(samples_train))
    print("Nombre d'images de val : %i" % len(samples_val))
    print("Nombre d'images de test : %i" % len(samples_test))


    # on définit d'autres dataset pytorch à partir des listes d'images de train / val / test
    dataset_train = datasets.ImageFolder(image_directory, data_transforms)
    dataset_train.samples = samples_train
    dataset_train.imgs = samples_train
    loader_train = torch.utils.data.DataLoader(dataset_train, batch_size=16, shuffle=True, num_workers=4)

    dataset_val = datasets.ImageFolder(image_directory, data_transforms)
    dataset_val.samples = samples_val
    dataset_val.imgs = samples_val

    dataset_test = datasets.ImageFolder(image_directory, data_transforms)
    dataset_test.samples = samples_test
    dataset_test.imgs = samples_test

    torch.manual_seed(42)




    # On remplace la dernière couche par une couche fully connected avec le bon nombre de classes

    convnet.to(device) # on utilise le GPU / CPU en fonction de ce qui est disponible

    convnet.train(True)

    # on définit une loss et un optimizer
    criterion,optimizer=configure_optimizer(convnet,optimizer)
    torch.manual_seed(42)
    train_model(convnet, loader_train, optimizer, criterion,device, n_epochs=epochs_nb)
    convnet.train(False)
    loss, accuracy = evaluate(convnet, dataset_test,device,criterion)
    print("Accuracy: %.1f%%" % (100 * accuracy))
    rmtree('temp_data_ft')

