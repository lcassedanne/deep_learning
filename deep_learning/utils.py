import os
from shutil import copyfile
import random
import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import models


def generate_sample_classes(k,datadir="../data/",target_dir="temp_data/"):
    subdirs = [el for el in os.listdir(datadir)]
    testing_subdirs = random.sample(subdirs,k)
    os.mkdir(target_dir)
    for el in testing_subdirs:
        all_files = os.listdir(datadir + el)
        os.mkdir(target_dir + el)
        for f in all_files:
            copyfile(datadir+el+"/"+f,target_dir+el+"/"+f)

def train_model(model, loader, optimizer, criterion,device, n_epochs=10,PRINT_LOSS=True):
    for epoch in range(n_epochs): # à chaque epochs
        print("EPOCH % i" % epoch)
        for i, data in enumerate(loader): # on itère sur les minibatchs via le loader
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device) # on passe les données sur CPU / GPU
            optimizer.zero_grad() # on réinitialise les gradients
            outputs = model(inputs) # on calcule l'output

            loss = criterion(outputs, labels) # on calcule la loss
            if PRINT_LOSS:
                print(loss.item())

            loss.backward() #on effectue la backprop pour calculer les gradients
            optimizer.step() #on update les gradients en fonction des paramètres

def evaluate(model, dataset,device,criterion):
    avg_loss = 0.
    avg_accuracy = 0
    loader = torch.utils.data.DataLoader(dataset, batch_size=16, shuffle=False, num_workers=2)
    for data in loader:
        inputs, labels = data
        inputs, labels = inputs.to(device), labels.to(device)
        outputs = model(inputs)

        loss = criterion(outputs, labels)
        _, preds = torch.max(outputs, 1)
        n_correct = torch.sum(preds == labels)

        avg_loss += loss.item()
        avg_accuracy += n_correct

    return avg_loss / len(dataset), float(avg_accuracy) / len(dataset)


def load_model(num_classes,name='resnet',fine_tuning=False):
    if torch.cuda.is_available():
        cuda = True
    else:
        cuda=False
    if name == 'resnet':
        if cuda:
            convnet = models.resnet152(pretrained=True)
        else:
            convnet = models.resnet18(pretrained=True)
        if not fine_tuning:
            # Seuls les paramètres de la dernière couche seront modifiées
            for param in convnet.parameters():
                param.requires_grad = False
        # modification de la dernière couche
        convnet.fc = nn.Linear(512, num_classes,bias=True)
    elif name == 'alexnet':
        convnet = models.alexnet(pretrained=True)
        if not fine_tuning:
            # Seuls les paramètres de la dernière couche seront modifiées
            for param in convnet.parameters():
                param.requires_grad = False
        # modification de la dernière couche
        convnet.classifier[6] = nn.Linear(4096,num_classes,bias=True)
    elif name=='inception':
        convnet = models.inception_v3(pretrained=true)
        if not fine_tuning:
            # Seuls les paramètres de la dernière couche seront modifiées
            for param in convnet.parameters():
                param.requires_grad = False
        # modification de la dernière couche
        convnet.AuxLogits.fc = nn.Linear(768, num_classes,bias=True)
        convnet.fc = nn.Linear(2048, num_classes,bias=True)
    elif name=='densenet':
        convnet = models.densenet161(pretrained=True)
        if not fine_tuning:
            # Seuls les paramètres de la dernière couche seront modifiées
            for param in convnet.parameters():
                param.requires_grad = False
        # modification de la dernière couche
        convnet.classifier = nn.Linear(1024, num_classes,bias=True)
    else:
        raise ValueError('Ce modèle n\'est pas disponible. Choisissez entre "resnet","alexnet","inception" et "densenet"')

    return convnet, cuda



def configure_optimizer(model,optim_name="SGD",lr=0.001,momentum=0.9):
    criterion = nn.CrossEntropyLoss()
    params_to_update = []
    for param in model.parameters():
        if param.requires_grad == True:
            params_to_update.append(param)
    if optim_name=="SGD":
        optimizer = optim.SGD(params_to_update, lr=lr, momentum=momentum)
    elif optim_name=="Adam":
        optimizer = optim.Adam(params_to_update, lr=lr)
    return criterion,optimizer


if __name__ == "__main__":
    generate_sample_classes(50)



